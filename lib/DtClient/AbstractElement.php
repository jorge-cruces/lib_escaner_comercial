<?php

namespace DSYDt;

class AbstractElement
{
    protected $client;
    protected $testMode;


    public function __construct($client){
        $this->client = $client;
    }

    public function setTestMode($testMode){
        $this->testMode = $testMode;
        return $this;
    }


}
<?php


namespace DSYDt;
use DSYDt\Elements\ProviderInfo;
use Goutte\Client;

/**
 * DtClient Scrapping
 * @package DSYDt
 * @author jaco <jorge.cruces@dsarhoya.cl>
 */
class DtClient extends Client
{
    private $testMode = true;



    public static function client(){

        $client = new self();

        return $client;
    }

    public function getProvider(){
        $provider = new ProviderInfo($this);
        $provider->setTestMode($this->testMode);
        return $provider;
    }
}
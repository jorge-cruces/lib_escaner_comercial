<?php


namespace App\MessageHandler;


use App\Message\CreateRutDB;
use App\Service\RutAnalysisService;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateRutDBHandler implements MessageHandlerInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var RutAnalysisService
     */
    private $rutAnalysisService;

    public function __construct(RutAnalysisService $rutAnalysisService)
    {

        $this->rutAnalysisService = $rutAnalysisService;
    }

    public function __invoke(CreateRutDB $createRutDB)
    {

        $rut = $createRutDB->getRut();
        $verifier = $createRutDB->getVerifier();
        $this->logger->info('Creando entrada DB RUT: ' . $rut . '-'.$verifier);
        $this->rutAnalysisService->analyseRut($rut,$verifier);

    }
}
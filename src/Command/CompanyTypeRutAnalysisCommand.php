<?php

namespace App\Command;

use App\Message\CreateRutDB;
use App\Repository\CompanyRegistryRepository;
use DSYDt\DtClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CompanyTypeRutAnalysisCommand extends Command
{
    protected static $defaultName = 'rutAnalysis';
    private static $commandSuccess = 0;
    private static $urlEndpoint = 'http://localhost:8003/api/1/rutanalysis';
    /**
     * @var CompanyRegistryRepository
     */
    private $companyRegistryRepository;
    /**
     * @var MessageBusInterface
     */
    private $messageBus;


    public function __construct(string $name = null, CompanyRegistryRepository $companyRegistryRepository,MessageBusInterface $messageBus)
    {
        parent::__construct($name);
        $this->companyRegistryRepository = $companyRegistryRepository;

        $this->messageBus = $messageBus;
    }

    protected function configure(){
        $this->setDescription('Create the data for a type of Company')
            ->setHelp('This command allows you get data')
            ->addArgument('type', InputArgument::REQUIRED,'Tipo de empresas');
    }

    protected function execute(InputInterface $input, OutputInterface $output){

        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException('This command accepts only an instance of "ConsoleOutputInterface".');
        }


        $companyTypeInput = $input->getArgument('type');

        // Una query grande con solo la columnna rut y verifier

        $companiesByType = $this->companyRegistryRepository->selectRutsByType($companyTypeInput);

        foreach($companiesByType as $company) {
            $rutMessage = new CreateRutDB($company['rutNumber'],$company['verifier']);
            $envelope = new Envelope($rutMessage, [
                new DelayStamp(500)
            ]);
            $this->messageBus->dispatch($envelope);
            $output->writeln('Rut : '. $company['rutNumber']. '-' . $company['verifier']. ' estado: Trabajo creado ');

        }


        return self::$commandSuccess;
    }






}
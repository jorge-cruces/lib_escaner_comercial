<?php

namespace App\Command;

use DSYDt\DtClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    protected static $defaultName = 'app:test-dt';
    private static $commandSuccess = 0;


    protected function configure(){
        $this->setDescription('Pull the data from a website of the goverment using a RUT')
            ->setHelp('This command allows you get data')
            ->addArgument('rut', InputArgument::REQUIRED,'RUT de la empresa a buscar');
    }

    protected function execute(InputInterface $input, OutputInterface $output){

        if (!$output instanceof ConsoleOutputInterface) {
            throw new \LogicException('This command accepts only an instance of "ConsoleOutputInterface".');
        }






        // Info estatica
        $table = new Table($output);
        $output->writeln("Buscando multas de la empresa con RUT: " . $input->getArgument('rut'));
        $table->setHeaders(['Procedencia','Multa','Estado','Fecha','NumFinal','Tipo','Enunciado']);

        //Info variable
        $this->getInfoDt($table,$input);



    }

    private function debugInfoDt($rut)
    {
        $dtClient = DtClient::client();
        $infoMultas = $dtClient->getProvider()->getInfoMultas($rut);
        return $infoMultas;
    }

    private function getInfoDt($table,$input){
        $dtClient = DtClient::client();
        $rutEmpresa = $input->getArgument('rut');
        $infoMultas = $dtClient->getProvider()->getInfoMultas($rutEmpresa);
        $table->setRows($infoMultas);
        $table->render();

    }

    private function formatInfoToTable($info){

        $dataTable = [];
        foreach($info as $key => $value)
        {
            $dataTable[] = [$value];
        }
        return $dataTable;
    }


}
<?php


namespace App\Controller\Api;

use App\Entity\DtFine;
use App\Entity\Rut;
use App\Entity\TestEntity;
use App\Form\RutQueryParametersType;
use App\Form\Type\Rut\RutType;

use App\Form\Type\Rut\RutTypeOwn;
use App\Message\CreateRutDB;
use App\Repository\DtFineRepository;
use App\Repository\RutRepository;
use App\Service\RutAnalysisService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/1")
 */
class RutController extends ApiBaseController
{
    /**
     * @Route("/rutanalysis",methods={"POST"})
     */
    public function rutAnalysisPOST(Request $request, MessageBusInterface $messageBus)
    {

        $parameters = json_decode($request->getContent(),true);
        $rut_str_complete = $parameters['rut'];
        $formRut = $this->createForm(RutTypeOwn::class);
        $isValidRut = $formRut->submit(['rut' => $rut_str_complete])->isValid();

        if($isValidRut){
            $rut_str = substr($rut_str_complete,0,-2);
            $numVerifier = substr($rut_str_complete,-1);

            $rutMessage = new CreateRutDB($rut_str,$numVerifier);
            $messageBus->dispatch($rutMessage);
            return new Response('',204);
        }
        //Bad request
        return new Response('',400);
    }


    // Implementar patrones de busqueda
    /*
     * fine_count_gt > greater than X
     * fine_count_lt < lower than X
     * payed_fine_count_gt > greater than X
     * payed_fine_count_lt < lower than X
     * not_payed_fine_count_gt > greater than X (mates)
     * not_payed_fine_count_lt < lower than X
     */

    /**
     * @Route("/ruts/",methods={"GET"})
     */
    public function rutsAllGET(Request $request, RutRepository $rutRepository): Response
    {

        $allDataRequest = $request->query->all();
        $formQueryParameters = $this->createForm(RutQueryParametersType::class);
        $isValidData = $formQueryParameters->submit($allDataRequest)->isValid();

        if($isValidData)
        {
            $array_ruts_query = $rutRepository->customQueryRut($request->query->all());
            return $this->serializedResponse($array_ruts_query,['rut_list']);
        }

        //Bad request
        return new Response('',400);

    }

    /**
     * @Route("/ruts/{id}")
     * @param Rut $rut
     * @return Response
     */
    public function rutOneGET(RutRepository $rutRepository,string $id): Response
    {

        $formRut = $this->createForm(RutTypeOwn::class);
        $isValidRut = $formRut->submit(['rut' => $id])->isValid();

        if($isValidRut){
            $rut_str = substr($id,0,-1);
            $rut_integer_db = (int) $rut_str;
            $rut = $rutRepository->findOneBy(['id'=> $rut_integer_db]);
            return $this->serializedResponse($rut,['rut_list']);
        }
        //Bad request
        return new Response('',400);

    }


}
<?php

namespace App\Form\Type\Rut;

use Symfony\Component\Form\DataTransformerInterface;
use App\Form\Type\Rut\RutFormatter;

/**
 * Description of RutTransformer
 *
 * @author mati
 */
class RutTransformer implements DataTransformerInterface
{
    
    /**
     * @var RutFormatterInterface
     */
    private $rutFormatter;

    public function __construct($addDots = false) {
        $this->rutFormatter = new RutFormatter($addDots);
    }
    
    /**
     * Transforma desde el modelo a formulario
     */
    public function transform($rut)
    {
        //Formatear el rut en caso de que esté mal formateado en la base de datos?
        return $rut;
    }

    /**
     * Transforma desde el formulario al modelo
     */
    public function reverseTransform($dirtyRut)
    {
        if (!$dirtyRut) {
            return;
        }
        return $this->rutFormatter->format($dirtyRut);
    }
}

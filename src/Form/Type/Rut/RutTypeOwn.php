<?php


namespace App\Form\Type\Rut;


use App\Validator\Constraints\RUT;
use App\Validator\Constraints\RUTValidator;
use Doctrine\DBAL\Types\IntegerType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RutTypeOwn extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('rut',RutType::class,[
            'constraints' => [new RUT()]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false,
            'allow_extra_fields' =>true
        ]);

    }

}
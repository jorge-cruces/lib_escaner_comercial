<?php

namespace App\Form\Type\Rut;

/**
 *
 * @author mati
 */
interface RutFormatterInterface {
    public function format($rut);
}

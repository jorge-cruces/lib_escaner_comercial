<?php

namespace App\Form;

use App\Entity\Rut;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

class RutQueryParametersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fine_count_gt',TextType::class,[
                'label' => false,
                'constraints' =>  [
                    new Regex([
                        'pattern' => '/^\d+$/'
                    ])
                ]
            ])
            ->add('fine_count_lt',TextType::class,[
                'label' => false,
                'constraints' =>  [
                    new Regex([
                        'pattern' => '/^\d+$/'
                    ])
                ]
            ])
            ->add('payed_fine_count_gt',TextType::class,[
                'label' => false,
                'constraints' =>  [
                    new Regex([
                        'pattern' => '/^\d+$/'
                    ])
                ]
            ])
            ->add('payed_fine_count_lt',TextType::class,[
                'label' => false,
                'constraints' =>  [
                    new Regex([
                        'pattern' => '/^\d+$/'
                    ])
                ]
            ])
            ->add('not_payed_fine_count_gt',TextType::class,[
                'label' => false,
                'constraints' =>  [
                    new Regex([
                        'pattern' => '/^\d+$/'
                    ])
                ]
            ])
            ->add('not_payed_fine_count_lt',TextType::class,[
                'label' => false,
                'constraints' =>  [
                    new Regex([
                        'pattern' => '/^\d+$/'
                    ])
                ]
            ]);



    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}

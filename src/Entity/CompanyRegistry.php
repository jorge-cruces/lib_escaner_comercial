<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

// 0 => "RUT"
// 1 => "DV"
// 2 => "Razón Social"
// 3 => "Tramo Ventas (5)"
// 4 => "N° Trabajadores (7,8)"
// 5 => "Rubro (4)"
// 6 => "Subrubro (4)"
// 7 => "Actividad Económica (4)"
// 8 => "Calle"
// 9 => "N°"
// 10 => "Bloque"
// 11 => "Depto."
// 12 => "Villa/Poblacion"
// 13 => "Comuna"
// 14 => "Región"
// 15 => "Fecha Inicio (9,10)"
// 16 => "Fecha Término Giro (11)"
// 17 => "Tipo Término Giro"
// 18 => "Tipo Contribuyente"
// 19 => "SubTipo Contribuyente"
// 20 => "F22 C 645 (12)"
// 21 => "F22 C 646 (12)"

/**
 * CompanyRegistry.
 *
 * @ORM\Table(name="company_registry", indexes={@ORM\Index(name="search_by_rut", columns={"rut_number"})})
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRegistryRepository")
 */
class CompanyRegistry
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="rut_number", type="integer")
     * @Assert\NotNull()
     */
    private $rutNumber;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $verifier;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotNull(message = "Este valor es obligatorio")
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $salesSection;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $workersCount;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $area;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $subArea;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $economicActivity;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $street;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $block;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $apartment;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $villa;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $locality;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $province;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $region;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $startDateString;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $activityEndDateString;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $activityEndType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $taxPayerType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $taxPayerSubType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $f22c6465;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $f22c6466;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $taxRegime;

    /**
     * @ORM\ManyToOne(targetEntity="CompanyRegistryYear")
     */
    private $year;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rutNumber.
     *
     * @param int $rutNumber
     *
     * @return CompanyRegistry
     */
    public function setRutNumber($rutNumber)
    {
        $this->rutNumber = $rutNumber;

        return $this;
    }

    /**
     * Get rutNumber.
     *
     * @return int
     */
    public function getRutNumber()
    {
        return $this->rutNumber;
    }

    /**
     * Set verifier.
     *
     * @param string $verifier
     *
     * @return CompanyRegistry
     */
    public function setVerifier($verifier)
    {
        $this->verifier = $verifier;

        return $this;
    }

    /**
     * Get verifier.
     *
     * @return string
     */
    public function getVerifier()
    {
        return $this->verifier;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return CompanyRegistry
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set salesSection.
     *
     * @param string $salesSection
     *
     * @return CompanyRegistry
     */
    public function setSalesSection($salesSection)
    {
        $this->salesSection = $salesSection;

        return $this;
    }

    /**
     * Get salesSection.
     *
     * @return string
     */
    public function getSalesSection()
    {
        return $this->salesSection;
    }

    /**
     * Set workersCount.
     *
     * @param int $workersCount
     *
     * @return CompanyRegistry
     */
    public function setWorkersCount($workersCount)
    {
        $this->workersCount = $workersCount;

        return $this;
    }

    /**
     * Get workersCount.
     *
     * @return int
     */
    public function getWorkersCount()
    {
        return $this->workersCount;
    }

    /**
     * Set area.
     *
     * @param string $area
     *
     * @return CompanyRegistry
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area.
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set subArea.
     *
     * @param string $subArea
     *
     * @return CompanyRegistry
     */
    public function setSubArea($subArea)
    {
        $this->subArea = $subArea;

        return $this;
    }

    /**
     * Get subArea.
     *
     * @return string
     */
    public function getSubArea()
    {
        return $this->subArea;
    }

    /**
     * Set economicActivity.
     *
     * @param string $economicActivity
     *
     * @return CompanyRegistry
     */
    public function setEconomicActivity($economicActivity)
    {
        $this->economicActivity = $economicActivity;

        return $this;
    }

    /**
     * Get economicActivity.
     *
     * @return string
     */
    public function getEconomicActivity()
    {
        return $this->economicActivity;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return CompanyRegistry
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set number.
     *
     * @param string $number
     *
     * @return CompanyRegistry
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number.
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set block.
     *
     * @param string $block
     *
     * @return CompanyRegistry
     */
    public function setBlock($block)
    {
        $this->block = $block;

        return $this;
    }

    /**
     * Get block.
     *
     * @return string
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Set apartment.
     *
     * @param string $apartment
     *
     * @return CompanyRegistry
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;

        return $this;
    }

    /**
     * Get apartment.
     *
     * @return string
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * Set villa.
     *
     * @param string $villa
     *
     * @return CompanyRegistry
     */
    public function setVilla($villa)
    {
        $this->villa = $villa;

        return $this;
    }

    /**
     * Get villa.
     *
     * @return string
     */
    public function getVilla()
    {
        return $this->villa;
    }

    /**
     * Set locality.
     *
     * @param string $locality
     *
     * @return CompanyRegistry
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality.
     *
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set region.
     *
     * @param string $region
     *
     * @return CompanyRegistry
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set startDateString.
     *
     * @param string $startDateString
     *
     * @return CompanyRegistry
     */
    public function setStartDateString($startDateString)
    {
        $this->startDateString = $startDateString;

        return $this;
    }

    /**
     * Get startDateString.
     *
     * @return string
     */
    public function getStartDateString()
    {
        return $this->startDateString;
    }

    /**
     * Get startDateString.
     *
     * @return string
     */
    public function getStartDate()
    {
        if (!$this->getStartDateString()) {
            return null;
        }

        preg_match('/\d{2}-\d{2}-\d{4}/', $this->getStartDateString(), $matches);
        if (count($matches)) {
            return \DateTime::createFromFormat('d-m-Y', $this->getStartDateString());
        }

        preg_match('/\d{4}-\d{2}-\d{2}/', $this->getStartDateString(), $matches);
        if (count($matches)) {
            return \DateTime::createFromFormat('Y-m-d', $this->getStartDateString());
        }

        return false;
    }

    /**
     * Set activityEndDateString.
     *
     * @param string $activityEndDateString
     *
     * @return CompanyRegistry
     */
    public function setActivityEndDateString($activityEndDateString)
    {
        $this->activityEndDateString = $activityEndDateString;

        return $this;
    }

    /**
     * Get activityEndDateString.
     *
     * @return string
     */
    public function getActivityEndDateString()
    {
        return $this->activityEndDateString;
    }

    /**
     * Set activityEndType.
     *
     * @param string $activityEndType
     *
     * @return CompanyRegistry
     */
    public function setActivityEndType($activityEndType)
    {
        $this->activityEndType = $activityEndType;

        return $this;
    }

    /**
     * Get activityEndType.
     *
     * @return string
     */
    public function getActivityEndType()
    {
        return $this->activityEndType;
    }

    /**
     * Set taxPayerType.
     *
     * @param string $taxPayerType
     *
     * @return CompanyRegistry
     */
    public function setTaxPayerType($taxPayerType)
    {
        $this->taxPayerType = $taxPayerType;

        return $this;
    }

    /**
     * Get taxPayerType.
     *
     * @return string
     */
    public function getTaxPayerType()
    {
        return $this->taxPayerType;
    }

    /**
     * Set taxPayerSubType.
     *
     * @param string $taxPayerSubType
     *
     * @return CompanyRegistry
     */
    public function setTaxPayerSubType($taxPayerSubType)
    {
        $this->taxPayerSubType = $taxPayerSubType;

        return $this;
    }

    /**
     * Get taxPayerSubType.
     *
     * @return string
     */
    public function getTaxPayerSubType()
    {
        return $this->taxPayerSubType;
    }

    /**
     * Set f22c6465.
     *
     * @param string $f22c6465
     *
     * @return CompanyRegistry
     */
    public function setF22c6465($f22c6465)
    {
        $this->f22c6465 = $f22c6465;

        return $this;
    }

    /**
     * Get f22c6465.
     *
     * @return string
     */
    public function getF22c6465()
    {
        return $this->f22c6465;
    }

    /**
     * Set f22c6466.
     *
     * @param string $f22c6466
     *
     * @return CompanyRegistry
     */
    public function setF22c6466($f22c6466)
    {
        $this->f22c6466 = $f22c6466;

        return $this;
    }

    /**
     * Get f22c6466.
     *
     * @return string
     */
    public function getF22c6466()
    {
        return $this->f22c6466;
    }

    /**
     * Set year.
     *
     * @param \AppBundle\Entity\CompanyRegistryYear $year
     *
     * @return CompanyRegistry
     */
    public function setYear(\AppBundle\Entity\CompanyRegistryYear $year = null)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year.
     *
     * @return \AppBundle\Entity\CompanyRegistryYear
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set province.
     *
     * @param string|null $province
     *
     * @return CompanyRegistry
     */
    public function setProvince($province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province.
     *
     * @return string|null
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set taxRegime.
     *
     * @param string|null $taxRegime
     *
     * @return CompanyRegistry
     */
    public function setTaxRegime($taxRegime = null)
    {
        $this->taxRegime = $taxRegime;

        return $this;
    }

    /**
     * Get taxRegime.
     *
     * @return string|null
     */
    public function getTaxRegime()
    {
        return $this->taxRegime;
    }

    /**
     * Set city.
     *
     * @param string|null $city
     *
     * @return CompanyRegistry
     */
    public function setCity($city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }
}

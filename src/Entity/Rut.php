<?php

namespace App\Entity;

use App\Repository\RutRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass=RutRepository::class)
 */
class Rut
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @JMS\SerializedName("rut")
     * @JMS\Groups({"rut_list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1)
     * @JMS\SerializedName("verifier")
     * @JMS\Groups({"rut_list"})
     */
    private $verifier;

    /**
     * @ORM\Column(type="integer")
     * @JMS\SerializedName("dt_fine_count")
     * @JMS\Groups({"rut_list"})
     */
    private $dt_fine_count;

    /**
     * @ORM\Column(type="integer")
     * @JMS\SerializedName("dt_fine_payed")
     * @JMS\Groups({"rut_list"})
     */
    private $dt_fine_payed_count;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\SerializedName("dt_fine_analyzed_at")
     * @JMS\Groups({"rut_list"})
     */
    private $dt_fine_analyzed_at;

    /**
     * @ORM\OneToMany(targetEntity=DtFine::class, mappedBy="Rut")
     * @JMS\Groups({"r_rut_dt_fine"})
     *
     */
    private $dtFines;

    public function __construct()
    {
        $this->dtFines = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }
    public function setId($id) : self
    {
        $this->id = $id;
        return $this;
    }

    public function getVerifier(): ?string
    {
        return $this->verifier;
    }

    public function setVerifier(string $verifier): self
    {
        $this->verifier = $verifier;

        return $this;
    }

    public function getDtFineCount(): ?int
    {
        return $this->dt_fine_count;
    }

    public function setDtFineCount(int $dt_fine_count): self
    {
        $this->dt_fine_count = $dt_fine_count;

        return $this;
    }

    public function getDtFinePayedCount(): ?int
    {
        return $this->dt_fine_payed_count;
    }

    public function setDtFinePayedCount(int $dt_fine_payed_count): self
    {
        $this->dt_fine_payed_count = $dt_fine_payed_count;

        return $this;
    }

    public function getDtFineAnalyzedAt(): ?\DateTimeInterface
    {
        return $this->dt_fine_analyzed_at;
    }

    public function setDtFineAnalyzedAt(?\DateTimeInterface $dt_fine_analyzed_at): self
    {
        $this->dt_fine_analyzed_at = $dt_fine_analyzed_at;

        return $this;
    }

    /**
     * @return Collection|DtFine[]
     */
    public function getDtFines(): Collection
    {
        return $this->dtFines;
    }

    public function addDtFine(DtFine $dtFine): self
    {
        if (!$this->dtFines->contains($dtFine)) {
            $this->dtFines[] = $dtFine;
            $dtFine->setRut($this);
        }

        return $this;
    }

    public function removeDtFine(DtFine $dtFine): self
    {
        if ($this->dtFines->removeElement($dtFine)) {
            // set the owning side to null (unless already changed)
            if ($dtFine->getRut() === $this) {
                $dtFine->setRut(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->id;
    }
}

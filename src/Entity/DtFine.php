<?php

namespace App\Entity;

use App\Repository\DtFineRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;


/**
 * @ORM\Entity(repositoryClass=DtFineRepository::class)
 */
class DtFine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Rut::class, inversedBy="dtFines")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\SerializedName("rut")
     * @JMS\Groups({"r_dt_fine_rut"})
     */
    private $Rut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\SerializedName("status_changed_at")
     * @JMS\Groups({"dt_fine_list"})
     */
    private $status_changed_at;

    /**
     * @ORM\Column(type="string", length=50)
     * @JMS\SerializedName("procedencia")
     * @JMS\Groups({"dt_fine_list"})
     */
    private $procedencia;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\SerializedName("multa")
     * @JMS\Groups({"dt_fine_list"})
     */
    private $multa;

    /**
     * @ORM\Column(type="string", length=50)
     * @JMS\SerializedName("estado")
     * @JMS\Groups({"dt_fine_list"})
     */
    private $estado;

    /**
     * @ORM\Column(type="string")
     * @JMS\SerializedName("fecha_ejecutoriedad")
     * @JMS\Groups({"dt_fine_list"})
     */
    private $original_fecha_ejecutoriedad;

    /**
     * @ORM\Column(type="date")
     * @JMS\Groups({"dt_fine_list"})
     */
    private $fecha_ejecutoriedad;

    /**
     * @ORM\Column(type="float")
     * @JMS\SerializedName("num_final")
     * @JMS\Groups({"dt_fine_list"})
     */
    private $num_final;

    /**
     * @ORM\Column(type="string", length=30)
     * @JMS\SerializedName("tipo_um")
     * @JMS\Groups({"dt_fine_list"})
     */
    private $tipo_um;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\SerializedName("enunciado")
     * @JMS\Groups({"dt_fine_list"})
     */
    private $enunciado;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRut(): ?Rut
    {
        return $this->Rut;
    }

    public function setRut(?Rut $Rut): self
    {
        $this->Rut = $Rut;

        return $this;
    }

    public function getStatusChangedAt(): ?\DateTimeInterface
    {
        return $this->status_changed_at;
    }

    public function setStatusChangedAt(?\DateTimeInterface $status_changed_at): self
    {
        $this->status_changed_at = $status_changed_at;

        return $this;
    }

    public function getProcedencia(): ?string
    {
        return $this->procedencia;
    }

    public function setProcedencia(string $procedencia): self
    {
        $this->procedencia = $procedencia;

        return $this;
    }

    public function getMulta(): ?string
    {
        return $this->multa;
    }

    public function setMulta(string $multa): self
    {
        $this->multa = $multa;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getFechaEjecutoriedad(): ?\DateTimeInterface
    {
        return $this->fecha_ejecutoriedad;
    }

    public function setFechaEjecutoriedad(?\DateTimeInterface $fecha_ejecutoriedad): self
    {
        $this->fecha_ejecutoriedad = $fecha_ejecutoriedad;

        return $this;
    }

    public function getNumFinal(): ?float
    {
        return $this->num_final;
    }

    public function setNumFinal(float $num_final): self
    {
        $this->num_final = $num_final;

        return $this;
    }

    public function getTipoUm(): ?string
    {
        return $this->tipo_um;
    }

    public function setTipoUm(string $tipo_um): self
    {
        $this->tipo_um = $tipo_um;

        return $this;
    }

    public function getEnunciado(): ?string
    {
        return $this->enunciado;
    }

    public function setEnunciado(string $enunciado): self
    {
        $this->enunciado = $enunciado;

        return $this;
    }


    public function getOriginalFechaEjecutoriedad() :?string
    {
        return $this->original_fecha_ejecutoriedad;
    }


    public function setOriginalFechaEjecutoriedad($original_fecha_ejecutoriedad): self
    {
        $this->original_fecha_ejecutoriedad = $original_fecha_ejecutoriedad;

        return $this;
    }

}

<?php


namespace App\Service;

use App\Entity\DtFine;
use App\Entity\Rut;
use App\Repository\DtFineRepository;
use App\Repository\RutRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use DSYDt\DtClient;

class RutAnalysisService
{
    private $provider;
    private $rutRespository;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var DtFineRepository
     */
    private $dtFineRepository;


    public function __construct(RutRepository $rutRepository, EntityManagerInterface $em, DtFineRepository $dtFineRepository)
    {
        $this->provider = DtClient::client()->getProvider();
        $this->rutRespository = $rutRepository;
        $this->em = $em;
        $this->dtFineRepository = $dtFineRepository;
    }

    public function analyseRut(string $rut_str, string $numVerfifier)
    {
        return $this->createRutColumDB($rut_str, $numVerfifier);
    }

    /**
     * Verifica si el rut existe en la DB
     * si existe actualiza los datos de la multa
     * sino crea el nuevo rut y crea los datos de la multa
     *
     * @param $rut_str
     * @param $numVerifier
     */
    private function createRutColumDB(string $rut_str, string $numVerifier): ?bool
    {
        $rut_query = $rut_str . '-' . $numVerifier;
        $rutExists = $this->rutRespository->rutExists($rut_str);
        $rut_integer_to_db = (int)$rut_str;

        if ($rutExists) {
            $rut_to_db = $this->rutRespository->findOneBy(['id' => $rut_integer_to_db]);
        } else {
            $rut_to_db = new Rut();
            $rut_to_db->setId($rut_integer_to_db);
            $rut_to_db->setVerifier($numVerifier);
        }

        $rut_to_db->setDtFineAnalyzedAt(new DateTime());
        return $this->createOrUpdateFineByRut($rut_query, $rut_to_db);
    }

    /**
     *
     * Crea las multas correspondiende al rut entregado
     * Si el rut ya existia entonces la actualiza.
     *
     * @param string $rut_str_complete
     * @param Rut $rut_to_db
     * @return null
     * @throws \Exception
     */
    private function createOrUpdateFineByRut(string $rut_str_complete, Rut $rut_to_db): ?bool
    {

        // Obtenemos todas las multas (si es que existen) ok
        // Las agregamos a la DB ok
        // Las agregamos a la relacion ok
        // Contamos las eñ total y la agregamos al rut ok
        // Contamos las pagas  y la agregamos al rut ok

        $array_multas = $this->provider->getInfoMultas($rut_str_complete);

        if ($array_multas === null) {
            return false;
        }

        // if Caso 0 multas
        // else hay multas
        if ($array_multas === 0) {
            $rut_to_db->setDtFineCount(0);
            $rut_to_db->setDtFinePayedCount(0);
        } else {

            // Una multa debe tener lo siguiente:
            /*
             * Rut | Relacion
             * procedencia
             * multa
             * estado
             * fecha
             * num_final
             * tipo_um
             * enunciado
             *
             */
            $num_multas_total = count($array_multas);
            $num_multad_payed = 0;
            // Verificar si existe la multa y actualizarla
            // Sino crear nueva multa y asociarla al rut
            foreach ($array_multas as $multa) {

                // Existe la multa en db
                if ($multa_to_db = $this->dtFineRepository->findOneBy(['multa' => $multa['multa']])) {

                    $previous_state = $multa_to_db->getEstado();
                    if ($previous_state !== $multa['estado']) {
                        $multa_to_db->setEstado($multa['estado']);
                        $multa_to_db->setStatusChangedAt(new DateTime());
                    }

                    if ($multa['estado'] === "PAGADA") {
                        $num_multad_payed += 1;
                    }
                } else {
                    $multa_to_db = new DtFine();
                    $multa_to_db->setRut($rut_to_db);
                    $multa_to_db->setProcedencia($multa['procedencia']);
                    $multa_to_db->setMulta($multa['multa']);
                    $multa_to_db->setEstado($multa['estado']);
                    $multa_to_db->setOriginalFechaEjecutoriedad($multa['fecha']);
                    $multa_to_db->setFechaEjecutoriedad(new DateTime($multa['fecha']));
                    $multa_to_db->setNumFinal((float)$multa['num']);
                    $multa_to_db->setTipoUm($multa['tipo']);
                    $multa_to_db->setEnunciado($multa['enunciado']);
                    if ($multa['estado'] === "PAGADA") {
                        $num_multad_payed += 1;
                    }
                }
                $this->em->persist($multa_to_db);
            }
            $rut_to_db->setDtFineCount($num_multas_total);
            $rut_to_db->setDtFinePayedCount($num_multad_payed);
        }


        $this->em->persist($rut_to_db);
        $this->em->flush();
        return true;
    }





}






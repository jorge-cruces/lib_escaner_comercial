<?php


namespace App\Message;


class CreateRutDB
{
    /**
     * @var string
     */
    private $rut;
    /**
     * @var string
     */
    private $verifier;

    public function __construct(string $rut,string $verifier)
    {

        $this->rut = $rut;
        $this->verifier = $verifier;
    }

    /**
     * @return string
     */
    public function getRut(): string
    {
        return $this->rut;
    }

    /**
     * @return string
     */
    public function getVerifier(): string
    {
        return $this->verifier;
    }
}
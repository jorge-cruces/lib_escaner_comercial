<?php

namespace App\Repository;

use App\Entity\DtFine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DtFine|null find($id, $lockMode = null, $lockVersion = null)
 * @method DtFine|null findOneBy(array $criteria, array $orderBy = null)
 * @method DtFine[]    findAll()
 * @method DtFine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DtFineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DtFine::class);
    }


}

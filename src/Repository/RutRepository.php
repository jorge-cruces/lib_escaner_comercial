<?php

namespace App\Repository;

use App\Entity\Rut;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Rut|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rut|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rut[]    findAll()
 * @method Rut[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RutRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rut::class);
    }

    /**
     * Devuelve true si el rut existe en la base de datos, caso contrario devuelve false.
     * @param int $rut
     * @return boolean
     */
    public function rutExists(string $rut) :?bool
    {
        $rut = $this->findOneBy(array('id' => $rut));
        return $rut !== null;
    }

    /**
     * CustomQuery para ruts
     * (1) fine_count_gt > greater than X
     * (2) fine_count_lt < lower than X
     * (3) payed_fine_count_gt > greater than X
     * (4) payed_fine_count_lt < lower than X
     * (5) not_payed_fine_count_gt > greater than X (mates)
     * (6) not_payed_fine_count_lt < lower than X
     *
     * Combinaciones validas
     * - Individuales
     * - 1 y 2
     * - 1 y 3
     * - 1 y 4
     * - 1
     *
     * @param $content
     * @return null
     */
    public function customQueryRut($content) {

        $qb = $this->createQueryBuilder('rut');

        $fine_count_gt = $content['fine_count_gt'] ?? null;
        $fine_count_lt = $content['fine_count_lt'] ?? null;
        $payed_fine_count_gt = $content['payed_fine_count_gt'] ?? null;
        $payed_fine_count_lt = $content['payed_fine_count_lt'] ?? null;
        $not_payed_fine_count_gt = $content['not_payed_fine_count_gt'] ?? null;
        $not_payed_fine_count_lt = $content['not_payed_fine_count_lt'] ?? null;

        if($fine_count_gt){

            $qb->andWhere('rut.dt_fine_count > :fine_count_gt');
            $qb->setParameter('fine_count_gt',$fine_count_gt);

        }
        if($fine_count_lt){
                $qb->andWhere('rut.dt_fine_count < :fine_count_lt');
            $qb->setParameter('fine_count_lt',$fine_count_lt);
        }

        if($payed_fine_count_gt){
            $qb->andWhere('rut.dt_fine_payed_count > :payed_fine_count_gt');
            $qb->setParameter('payed_fine_count_gt',$payed_fine_count_gt);
        }

        if($payed_fine_count_lt){
            $qb->andWhere('rut.dt_fine_payed_count < :payed_fine_count_lt');
            $qb->setParameter('payed_fine_count_lt',$payed_fine_count_lt);
        }

        if($not_payed_fine_count_gt){
            $qb->andWhere('rut.dt_fine_count - rut.dt_fine_payed_count > :not_payed_fine_count_gt');
            $qb->setParameter('not_payed_fine_count_gt',$not_payed_fine_count_gt);
        }

        if($not_payed_fine_count_lt){
            $qb->andWhere('rut.dt_fine_count - rut.dt_fine_payed_count < :not_payed_fine_count_lt');
            $qb->setParameter('not_payed_fine_count_lt',$not_payed_fine_count_lt);

        }




        $query = $qb->getQuery();
        return $query->execute();
    }

}

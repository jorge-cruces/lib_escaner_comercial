<?php

namespace App\Repository;

use App\Entity\CompanyRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompanyRegistry|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyRegistry|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyRegistry[]    findAll()
 * @method CompanyRegistry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRegistryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyRegistry::class);
    }

    public function selectRutsByType(string $type){
        $qb = $this->createQueryBuilder('company')
            ->select('company.rutNumber,company.verifier')
            ->where('company.salesSection = :type')
            ->setParameter('type',$type);
        $query = $qb->getQuery();
        return $query->execute();
    }


}

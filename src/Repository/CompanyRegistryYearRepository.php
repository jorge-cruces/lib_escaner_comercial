<?php

namespace App\Repository;

use App\Entity\CompanyRegistryYear;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompanyRegistryYear|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyRegistryYear|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyRegistryYear[]    findAll()
 * @method CompanyRegistryYear[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRegistryYearRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyRegistryYear::class);
    }


}

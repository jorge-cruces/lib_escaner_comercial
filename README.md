# Instalacion 
Luego del git clone ejecutar:
```
composer install
```
- Cambiar configuración de la base de datos dentro .env
- Tener una base de datos corriendo
# Correr el contenedor 
```
COMPOSE_PORT=8031 COMPOSE_PROJECT_NAME=lib docker-compose -f docker-compose.yml -f docker-compose-local.yml up -d
```

# Consumir la cola de trabajos

### De forma local o para testing
Correr el worker de la forma
```
./bin/console messenger:consume -vv
``` 

### Produccion
1. Cambiar path de supervisor del archivo 
config/messenger-worker.ini a la carpeta donde se encuentra el proyecto.
2. Crear un symlink en la carpeta de config
del supervisor,
   ```
   ln -s ~/Sites/messenger/config/messenger-worker.ini /carpeta/config/supervisor
``
3. Hacemos que supervisor lea los cambios
```
supervisorctl -c /carpeta/config/supervisorreread
``` 
4. Updateamos supervisor 
```
supervisorctl -c /usr/local/etc/supervisord.ini update
``` 
5. Echamos a correr supervisor 
```
supervisorctl -c /usr/local/etc/supervisord.ini start messenger-consume:*
``` 



# Modo de uso 

### Comandos a ejecutar
```
./bin/console app:test-dt RUT
```
Comando que devuelve una lista de multas
del rut ingresado (RUT con formato: 12345678-9)

```
./bin/console rutanalysis N 
```
Comando que analiza una lista de empresas del tipo
N, con N del 1 al 9

### Api v1 

- POST api/v1/rutanalysis
    -  Crea una analisis de un rut entregado en el body con formato json de la forma
    {"rut": "12345678-9"}. Guarda el rut en la base de datos y lo ofrece en forma de API.

- GET api/v1/ruts
    - Entrega todos los ruts disponibles en la base de datos. Opciones para hacer la query se ponen de parametros (E.g :api/v1/ruts/?fine_count_gt=3):
        - fine_count_gt > greater than X
        - fine_count_lt < lower than X
        - payed_fine_count_gt > greater than X
        - payed_fine_count_lt < lower than X
        - not_payed_fine_count_gt > greater than X 
        - not_payed_fine_count_lt < lower than X

- GET api/v1/ruts/{id}
    -  Obtiene un rut en específico utilizando el id entregado. Formato rut
    123456789 o 12345678-9 con o sin verificador.
       Eg : api/v1/ruts/760145017
    
    

       



